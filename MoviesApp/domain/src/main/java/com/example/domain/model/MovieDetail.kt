package com.example.domain.model

/**
 * Model dos detalhes
 * */
data class MovieDetail(
    val backPosterUrl: String,
    val overview: String,
    val voteAverage: Double,
    val genres: List<String>?,
    val id: Int,
    val title: String,
    val isFavorite: Boolean,
    val posterUrl: String,
    val releaseDate: String
)
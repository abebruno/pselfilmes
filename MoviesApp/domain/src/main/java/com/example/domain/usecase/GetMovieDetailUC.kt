package com.example.domain.usecase

import com.example.domain.datarepository.MovieDataRepository
import com.example.domain.model.MovieDetail
import io.reactivex.Single
import javax.inject.Inject

class GetMovieDetailUC @Inject constructor(private val movieDataRepository: MovieDataRepository) {
    fun getSingle(movieId: Int): Single<MovieDetail> = movieDataRepository.getMovieDetails(movieId)
}
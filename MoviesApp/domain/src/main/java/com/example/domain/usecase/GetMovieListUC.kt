package com.example.domain.usecase

import com.example.domain.datarepository.MovieDataRepository
import com.example.domain.model.Movie
import io.reactivex.Single
import javax.inject.Inject

class GetMovieListUC @Inject constructor(private val movieDataRepository: MovieDataRepository) {
    fun getSingle(): Single<List<Movie>> = movieDataRepository.getMovieList()
}
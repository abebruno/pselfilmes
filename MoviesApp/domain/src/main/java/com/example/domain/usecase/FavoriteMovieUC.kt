package com.example.domain.usecase

import com.example.domain.datarepository.MovieDataRepository
import io.reactivex.Completable
import javax.inject.Inject

class FavoriteMovieUC @Inject constructor(private val movieDataRepository: MovieDataRepository) {
    fun getCompletable(movieId: Int): Completable = movieDataRepository.favoriteMovie(movieId)
}
package com.example.domain.model

/**
 * Model do filme
 * */
data class Movie(
    val id: Int,
    val title: String,
    var isFavorite: Boolean = false,
    val posterUrl: String,
    val releaseDate: String)
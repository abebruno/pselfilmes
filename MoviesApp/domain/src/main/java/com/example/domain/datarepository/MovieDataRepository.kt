package com.example.domain.datarepository

import com.example.domain.model.Movie
import com.example.domain.model.MovieDetail
import io.reactivex.Completable
import io.reactivex.Single

interface MovieDataRepository{
    fun getMovieList(): Single<List<Movie>>
    fun getMovieDetails(id: Int): Single<MovieDetail>
    fun favoriteMovie(movieId: Int): Completable
    fun unfavoriteMovie(movieId: Int): Completable
    fun getFavoriteList(): Single<MutableList<Int>>
}
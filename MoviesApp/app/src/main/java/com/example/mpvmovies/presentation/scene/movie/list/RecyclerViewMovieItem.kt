package com.example.mpvmovies.presentation.scene.movie.list

import com.bumptech.glide.Glide
import com.example.mpvmovies.R
import com.jakewharton.rxbinding3.view.clicks
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.movie_list_recycler_view_layout.view.*

class RecyclerViewMovieItem(private val movie: MovieVM) : Item(){
    var itemClicks: PublishSubject<MovieVM> = PublishSubject.create() //Subject que vai ser usado no RxBind
    var favItemClicks: PublishSubject<MovieVM> = PublishSubject.create()

    //Diz qual layout será usado
    override fun getLayout(): Int {
        return R.layout.movie_list_recycler_view_layout
    }
    //Faz o bind
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val itemView = viewHolder.itemView
        itemView.titleTextView.text = movie.title
        itemView.releaseTextView.text = movie.fixDate(movie.releaseDate)

        //Diz qual layout será usado
        Glide.with(itemView.context).load(movie.posterUrl).error(R.drawable.no_image).into(itemView.posterImageView)

        //Verifica se é um favorito
        if(movie.isFavorite)
            Glide.with(itemView.context).load(R.drawable.baseline_favorite_black_48).error(R.drawable.baseline_favorite_border_black_48).into(itemView.favImageView)
        else
            Glide.with(itemView.context).load(R.drawable.baseline_favorite_border_black_48).error(R.drawable.baseline_favorite_border_black_48).into(itemView.favImageView)

        //Faz a lógica do clique do botão de favorito
        itemView.favImageView.clicks().map{ movie }
            .doOnNext {
                if(!movie.isFavorite)
                    Glide.with(itemView.context).load(R.drawable.baseline_favorite_black_48).error(R.drawable.baseline_favorite_border_black_48).into(itemView.favImageView)
                else
                    Glide.with(itemView.context).load(R.drawable.baseline_favorite_border_black_48).error(R.drawable.baseline_favorite_border_black_48).into(itemView.favImageView)
            }.subscribe(favItemClicks)

        //Mapeia o Unit, que é o padrão da view, para Movie e seta o subscribe, vai agir como observable
        itemView.posterImageView.clicks().map { movie }.subscribe(itemClicks)
    }
}
package com.example.mpvmovies.presentation.common

import java.io.Serializable

sealed class ViewResponse<out T : Serializable> : Serializable {
    class Success<out T : Serializable>(val data: T) : ViewResponse<T>()
    class Error(val error: ErrorVM) : ViewResponse<Nothing>()
}
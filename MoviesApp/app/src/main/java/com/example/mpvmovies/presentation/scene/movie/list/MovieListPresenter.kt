package com.example.mpvmovies.presentation.scene.movie.list


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.example.domain.usecase.FavoriteMovieUC
import com.example.domain.usecase.GetMovieListUC
import com.example.domain.usecase.UnfavoriteMovieUC
import com.example.mpvmovies.R
import com.example.mpvmovies.data.repository.Repository
import com.example.mpvmovies.presentation.common.*
import com.example.mpvmovies.presentation.common.livedata.StateEventLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MovieListPresenter : Fragment(), BackButtonListener {
    companion object {
        fun newInstance(): MovieListPresenter = MovieListPresenter()
    }

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var getMovieListUC: GetMovieListUC

    @Inject
    lateinit var favoriteMovieUC: FavoriteMovieUC

    @Inject
    lateinit var unfavoriteMovieUC: UnfavoriteMovieUC

    @Inject
    lateinit var ui: MovieListUI

    private val onViewCreatedObservable: PublishSubject<Unit> = PublishSubject.create()

    private val loading = StateEventLiveData<Boolean>()

    var movieList: StateEventLiveData<ViewResponse<ArrayList<MovieVM>>> = StateEventLiveData() //Usa o arrayList pois é serializável ;)

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_list_presenter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectFields()
        sceneUtils()
        observeViewState(lifecycle)

        onViewCreatedObservable.onNext(Unit)
    }

    private fun observeViewState(lifecycle: Lifecycle) {
        movieList.observeStateEvent(lifecycle, Observer {
            when (it.data) {
                is ViewResponse.Success -> ui.showMovies(it.data.data)
                is ViewResponse.Error -> ui.showError()
            }
        })

        loading.observeStateEvent(lifecycle, Observer { stateEvent ->
            if(stateEvent.data){
                ui.showProgress()
            }else{
                ui.hideProgress()
            }
        })
    }

    private fun sceneUtils(){
        onViewCreatedObservable.mergeWith(ui.onTryAgainObservable)
            .doOnNext { loading.postEvent(true) }
            .flatMapCompletable {
                getMovieListUC.getSingle()
                    .map { list -> list.toVM().toArrayList() }
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSuccess { moviesList ->
                        movieList.postEvent(ViewResponse.Success(moviesList))
                    }
                    .doOnError{ error ->
                        movieList.postEvent(ViewResponse.Error(error.toErrorVM(true)))
                    }
                    .doFinally{
                        loading.postEvent(false)
                    }
                    .ignoreElement()
                    .onErrorComplete()
            }.subscribe().addTo(compositeDisposable)

        ui.onMovieSelectedObservable.subscribe{ movie ->
            navigateToDetails(movie.id)
        }.addTo(compositeDisposable)

        ui.onFavoriteMovieObservable
            .flatMapCompletable{ movie ->
                if(movie.isFavorite){
                    movie.isFavorite = false
                    unfavoriteMovieUC.getCompletable(movie.id)
                }else{
                    movie.isFavorite = true
                    favoriteMovieUC.getCompletable(movie.id)
                }
            }
            .onErrorComplete{ error ->
                Log.d("HelpMe", error.toString())
                true
            }
            .subscribe().addTo(compositeDisposable)
    }

    private fun injectFields(){
        val component =
            DaggerMoviesListComponent
            .builder()
            .applicationComponent((activity?.application as? MoviesApplication)?.applicationComponent)
            .moviesListModule(activity?.let { fragmentActivity -> MoviesListModule(fragmentActivity)})
            .build()
        component.inject(this)
    }

    private fun navigateToDetails(movieId: Int) {
        router.navigateTo(NavigationKeys.MovieDetails(movieId))
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
    }
}

package com.example.mpvmovies.presentation.scene.movie.list

import com.example.domain.model.Movie

fun List<Movie>.toVM(): List<MovieVM> = map { movie -> movie.toVM() }

fun <T> List<T>.toArrayList() = ArrayList<T>(this)

fun Movie.toVM(): MovieVM = MovieVM(id, title, isFavorite, posterUrl, releaseDate)
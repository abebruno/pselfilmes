package com.example.mpvmovies.presentation.scene.movie.list

import java.io.Serializable

data class MovieVM (
    val id: Int,
    val title: String,
    var isFavorite: Boolean = false,
    val posterUrl: String,
    val releaseDate: String): Serializable {

        fun fixDate(date: String): String {
            val aux = date.split("-")

            if (aux.size == 3)
                return (aux[2] + "-" + aux[1] + "-" + aux[0])

            return date
        }
}
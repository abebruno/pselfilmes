package com.example.mpvmovies.presentation.common.di

import android.content.Context
import com.example.domain.datarepository.MovieDataRepository
import com.example.mpvmovies.data.cache.MovieCDS
import com.example.mpvmovies.data.remote.MovieRDS
import com.example.mpvmovies.data.repository.Repository
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.pacoworks.rxpaper2.RxPaperBook
import dagger.Component
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router


@Module
class ApplicationModule(private val context: Context) {
    @Provides
    @AppScope
    fun getContext(): Context = context

    @Provides
    @AppScope
    fun getCicerone(): Cicerone<Router> = Cicerone.create()


    @Provides
    @AppScope
    fun getNavigationHolder(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder


    @Provides
    @AppScope
    fun getRouter(cicerone: Cicerone<Router>): Router = cicerone.router

    @Provides
    @AppScope
    fun getRetrofit(): Retrofit {
        val baseUrl = "https://desafio-mobile.nyc3.digitaloceanspaces.com/"
        return Retrofit
            .Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    @Provides
    @AppScope
    fun getRemoteDataSource(retrofit: Retrofit): MovieRDS = retrofit.create(MovieRDS::class.java)

    @Provides
    @AppScope
    fun getCacheManager(context: Context): MovieCDS {
        RxPaperBook.init(context)

        return MovieCDS(RxPaperBook.with())
    }

    @Provides
    @AppScope
    fun movieDataRepository(movieRepository: Repository): MovieDataRepository = movieRepository
}


@Component(modules = [ApplicationModule::class])
@AppScope
interface ApplicationComponent {
    fun getCicerone(): Cicerone<Router>
    fun getNavigationHolder(): NavigatorHolder
    fun getRouter(): Router
    fun getRepository(): MovieDataRepository
    fun getRemoteDataSource(): MovieRDS
    fun getCacheManager(): MovieCDS
}
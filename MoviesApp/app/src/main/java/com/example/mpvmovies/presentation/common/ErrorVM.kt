package com.example.mpvmovies.presentation.common

import java.io.Serializable

sealed class ErrorVM(open val isBlocking: Boolean) : Serializable {
    class Generic(override val isBlocking: Boolean) : ErrorVM(isBlocking)
}

fun Throwable.toErrorVM(isBlocking: Boolean): ErrorVM = ErrorVM.Generic(isBlocking)

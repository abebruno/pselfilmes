package com.example.mpvmovies.data.repository

import com.example.domain.datarepository.MovieDataRepository
import com.example.domain.model.Movie
import com.example.domain.model.MovieDetail
import com.example.mpvmovies.data.cache.MovieCDS
import com.example.mpvmovies.data.cache.model.MovieCM
import com.example.mpvmovies.data.mapper.toDM
import com.example.mpvmovies.data.mapper.toMovieCM
import com.example.mpvmovies.data.mapper.toMovieDM
import com.example.mpvmovies.data.remote.MovieRDS
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class Repository @Inject constructor(private val movieCDS: MovieCDS, private val movieRDS: MovieRDS): MovieDataRepository {
    override fun getMovieDetails(id: Int): Single<MovieDetail> {
        return Single.zip(movieRDS.getDetails(id.toString()), movieCDS.getFavoriteMovieList(), BiFunction { movieDetailRM, favoriteList ->
            if(favoriteList.contains(movieDetailRM.id))
                movieDetailRM.toDM(true)
            else
                movieDetailRM.toDM(false)
        })
    }

    private fun getMovieListAux(): Single<List<MovieCM>>{
        return movieCDS.getCachedData()
            .onErrorResumeNext{
                movieRDS.getMovies()
                    .map { list -> list.toMovieCM() }
                    .flatMap {listCM ->
                        movieCDS.setCachedData(listCM)
                            .toSingleDefault(listCM)
                    }
            }
    }

    /**
     * flatMap -> quando há uma dependencia entre os observáveis
     * zip -> quando um precisa do outro, mas sem dependencia para conseguir o resultado
     * */
    override fun getMovieList(): Single<List<Movie>>{
        return Single.zip(getMovieListAux() ,movieCDS.getFavoriteMovieList(), BiFunction {
                movieList, favoriteList -> movieList.toMovieDM(favoriteList) } )
    }

    override fun getFavoriteList(): Single<MutableList<Int>>{
        return movieCDS.getFavoriteMovieList()
    }

    override fun favoriteMovie(movieId: Int): Completable{
        return movieCDS.getFavoriteMovieList().flatMapCompletable {favoriteList ->
            if(!favoriteList.contains(movieId))
                favoriteList.add(movieId)

            movieCDS.favoriteMovie(favoriteList)
        }
    }

    override fun unfavoriteMovie(movieId: Int): Completable {
        return movieCDS.getFavoriteMovieList().flatMapCompletable {favoriteList ->
            if(favoriteList.contains(movieId))
                favoriteList.remove(movieId)

            movieCDS.favoriteMovie(favoriteList)
        }
    }
}
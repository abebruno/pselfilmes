package com.example.mpvmovies.presentation.common

import androidx.fragment.app.Fragment
import com.example.mpvmovies.presentation.scene.fake.FakeFragment
import com.example.mpvmovies.presentation.scene.movie.detail.MovieDetailPresenter
import com.example.mpvmovies.presentation.scene.movie.list.MovieListPresenter
import ru.terrakok.cicerone.android.support.SupportAppScreen

class NavigationKeys {
    class MoviesList : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return MovieListPresenter.newInstance()
        }
    }

    class Fake : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return FakeFragment()
        }
    }

    class MovieDetails(private val movieId: Int) : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return MovieDetailPresenter.newInstance(movieId)
        }
    }

}
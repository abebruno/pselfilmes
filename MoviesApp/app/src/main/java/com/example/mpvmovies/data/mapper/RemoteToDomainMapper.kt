package com.example.mpvmovies.data.mapper

import com.example.domain.model.MovieDetail
import com.example.mpvmovies.data.remote.model.MovieDetailRM

fun MovieDetailRM.toDM(isFavorite: Boolean) = MovieDetail(backPosterUrl, overview, voteAverage, genres, id, title, isFavorite, posterUrl, releaseDate)
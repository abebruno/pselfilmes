package com.example.mpvmovies.presentation.scene.movie.list

import androidx.fragment.app.FragmentActivity
import com.example.mpvmovies.presentation.common.di.ActivityScope
import com.example.mpvmovies.presentation.common.di.ApplicationComponent

import dagger.Component
import dagger.Module
import dagger.Provides

@Module
class MoviesListModule(private val frameActivity: FragmentActivity){
    @Provides
    fun getMovieListUI(view: MovieListView): MovieListUI = view

    @Provides
    fun getFrameActivity(): FragmentActivity = frameActivity

}

@Component(modules = [MoviesListModule::class], dependencies = [ApplicationComponent::class])
@ActivityScope
interface MoviesListComponent{
    fun inject(moviesListPresenter: MovieListPresenter)
}
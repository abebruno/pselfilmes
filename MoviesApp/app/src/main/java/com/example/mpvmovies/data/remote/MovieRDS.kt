package com.example.mpvmovies.data.remote

import com.example.mpvmovies.data.remote.model.MovieDetailRM
import com.example.mpvmovies.data.remote.model.MovieRM
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieRDS{
    @GET("movies")
    fun getMovies(): Single<List<MovieRM>>

    @GET("movies/{id}")
    fun getDetails(@Path("id") id: String): Single<MovieDetailRM>
}
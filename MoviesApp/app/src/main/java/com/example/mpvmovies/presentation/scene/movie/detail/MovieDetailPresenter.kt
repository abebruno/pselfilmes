package com.example.mpvmovies.presentation.scene.movie.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.evernote.android.state.State
import com.example.domain.usecase.GetMovieDetailUC
import com.example.mpvmovies.R
import com.example.mpvmovies.presentation.common.*
import com.example.mpvmovies.presentation.common.livedata.StateEventLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import ru.terrakok.cicerone.Router
import javax.inject.Inject


class MovieDetailPresenter : Fragment(), BackButtonListener {
    companion object {
        fun newInstance(id: Int): MovieDetailPresenter = MovieDetailPresenter().apply { movieId = id }
    }

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var getMovieDetailUC: GetMovieDetailUC

    @Inject
    lateinit var ui: MovieDetailUI

    @State
    var movieId = -1

    @State(BundlerLiveData::class)
    var movie: StateEventLiveData<ViewResponse<MovieDetailVM>> = StateEventLiveData()

    private val onViewCreatedObservable: PublishSubject<Int> = PublishSubject.create()

    private val loading = StateEventLiveData<Boolean>()

    var compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_detail_presenter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        injectDependencies()
        sceneUtilsLD()
        observeViewState(lifecycle)

        onViewCreatedObservable.onNext(movieId)
    }

    private fun observeViewState(lifecycle: Lifecycle) {
        movie.observeStateEvent(lifecycle, Observer {
            when (it.data) {
                is ViewResponse.Success -> ui.showDataInScreen(it.data.data)
                is ViewResponse.Error -> ui.showError()
            }
        })

        loading.observeStateEvent(lifecycle, Observer { stateEvent ->
            if(stateEvent.data){
                ui.showProgress()
            }else{
                ui.hideProgress()
            }
        })
    }

    private fun injectDependencies(){
        val component = DaggerMovieDetailComponent
            .builder()
            .applicationComponent((activity?.application as? MoviesApplication)?.applicationComponent)
            .movieDetailModule(activity?.let { fragmentActivity -> MovieDetailModule(fragmentActivity) })
            .build()

        component.inject(this)
    }

    private fun sceneUtilsLD(){
        onViewCreatedObservable.mergeWith(ui.onTryAgainObservable)
            .doOnNext { loading.postEvent(true) }
            .flatMapCompletable {
                getMovieDetailUC.getSingle(movieId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .doOnSuccess { movie.postEvent(ViewResponse.Success(it.toVM())) }
                    .doOnError { movie.postEvent(ViewResponse.Error(it.toErrorVM(true))) }
                    .doFinally { loading.postEvent(false) }
                    .ignoreElement().onErrorComplete()
            }.subscribe().addTo(compositeDisposable)
    }

    override fun onBackPressed(): Boolean {
        router.exit()

        compositeDisposable.dispose()
        compositeDisposable = CompositeDisposable()
        return true
    }
}

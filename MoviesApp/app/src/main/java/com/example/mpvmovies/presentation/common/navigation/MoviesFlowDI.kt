package com.example.mpvmovies.presentation.common.navigation

import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.example.mpvmovies.R
import com.example.mpvmovies.presentation.common.di.ActivityScope
import com.example.mpvmovies.presentation.common.di.ApplicationComponent
import dagger.Component
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class MoviesFlowModule(private val frameActivity: FragmentActivity, private val fragmentManager: FragmentManager){
    @Provides
    @ActivityScope
    fun getNavigator(): Navigator {
        return SupportAppNavigator(frameActivity,  fragmentManager, R.id.containerBaseMovieList)
    }
}

@Component(modules = [MoviesFlowModule::class], dependencies = [ApplicationComponent::class])
@ActivityScope
interface MoviesFlowComponent{
    fun inject(moviesFlowFragment: MoviesFlowFragment)
}
package com.example.mpvmovies.data.cache.model

import com.google.gson.annotations.SerializedName

data class MovieCM(
    val id: Int,

    val title: String,

    @SerializedName("poster_url")
    val posterUrl: String,

    @SerializedName("release_date")
    val releaseDate: String
)
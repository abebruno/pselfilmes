package com.example.mpvmovies.presentation.common

interface BackButtonListener {
    fun onBackPressed(): Boolean
}
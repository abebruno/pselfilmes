package com.example.mpvmovies.presentation.scene.movie.list

import android.app.AlertDialog
import android.os.Handler
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_movie_list_presenter.*
import javax.inject.Inject
import kotlin.system.exitProcess

class MovieListView @Inject constructor(private val activity: FragmentActivity): MovieListUI {
    override val onTryAgainObservable: PublishSubject<Unit> = PublishSubject.create()
    override val onFavoriteMovieObservable: PublishSubject<MovieVM> = PublishSubject.create()
    override val onMovieSelectedObservable: PublishSubject<MovieVM> = PublishSubject.create()

    private var handler: Handler? = null //Handler é tipo uma thread, só que tem acesso a UI, ideal para mostrar progressos!
    private var progressStatus: Int = 0
    private var isStarted: Boolean = false

    override fun showMovies(movie: List<MovieVM>) {
        activity.movieListRecyclerView.layoutManager = LinearLayoutManager(activity)
        val adapter = GroupAdapter<ViewHolder>()

        //Coloca todos os filmes no adapter, além do clickListener
        movie.forEach {
            val item = RecyclerViewMovieItem(it).apply {
                itemClicks.subscribe(onMovieSelectedObservable)
                favItemClicks.subscribe(onFavoriteMovieObservable) //ACHO que o problema é aqui

            }
            adapter.add(item)
        }
        activity.movieListRecyclerView.adapter = adapter
    }

    override fun showProgress() {
        //utiliza o handler para mostrar o progressBar
        handler = Handler(Handler.Callback {
            if (isStarted) {
                progressStatus++
            }
            activity.progressBar.progress = this.progressStatus
            handler?.sendEmptyMessageAtTime(0, 100) //manda uma mensagem dizendo que tudo está certo
            true
        })
    }

    override fun hideProgress() {
        activity.progressBar.visibility = View.INVISIBLE
        isStarted = false
    }

    override fun showError() {
        AlertDialog.Builder(activity)
            .setTitle("Algo deu errado :(")
            .setMessage("Deseja tentar novamente?")
            .setPositiveButton("Sim") { _, _ ->
                onTryAgainObservable.onNext(Unit)
            }.setNegativeButton("Não") { _, _->
                exitProcess(0)
            }.create().show()
    }
}
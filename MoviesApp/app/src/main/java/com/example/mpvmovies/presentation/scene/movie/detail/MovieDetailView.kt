package com.example.mpvmovies.presentation.scene.movie.detail

import android.app.AlertDialog
import android.os.Handler
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.example.mpvmovies.R
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_movie_detail_presenter.*
import javax.inject.Inject
import kotlin.system.exitProcess

class MovieDetailView @Inject constructor(private val activity: FragmentActivity): MovieDetailUI{
    override val onTryAgainObservable: PublishSubject<Int> = PublishSubject.create()

    private var handler: Handler? = null //Handler é tipo uma thread, só que tem acesso a UI, ideal para mostrar progressos!
    private var progressStatus: Int = 0
    private var isStarted: Boolean = false
    private var movieId: Int = 0

    override fun showDataInScreen(details: MovieDetailVM) {
        movieId = details.id
        activity.detailNameAverageTextView.text = "Avaliação"
        activity.detailNameGenreTextView.text = "Gêneros"
        activity.detailReleaseNameTextView.text = "Lançamento"
        activity.detailTitleTextView.text = details.title
        activity.detailGenreTextView.text = details.genres.toString().replace("[", "").replace("]", "") //Arruma a data
        activity.detailReleaseTextView.text = details.releaseDate
        activity.detailAverageTextView.text = details.voteAverage.toString()
        activity.detailOverviewTextView.text = details.overview

        Glide.with(activity.applicationContext).load(details.posterUrl).error(R.drawable.no_image).into(activity.detailImageView)
        Glide.with(activity.applicationContext).load(details.backPosterUrl).error(R.drawable.no_image)
            .into(activity.backDetailImageView)

        if(details.isFavorite)
            Glide.with(activity.applicationContext).load(R.drawable.baseline_favorite_black_48).error(R.drawable.no_image).into(activity.favImageViewDetails)

    }

    override fun hideProgress() {
        activity.progressBarDetail.visibility = View.INVISIBLE
        isStarted = false
    }

    override fun showProgress() {
        //utiliza o handler para mostrar o progressBar
        handler = Handler(Handler.Callback {
            if (isStarted) {
                progressStatus++
            }
            activity.progressBarDetail.progress = this.progressStatus
            handler?.sendEmptyMessageAtTime(0, 100) //manda uma mensagem dizendo que tudo está certo
            true
        })
    }

    override fun showError() {
        AlertDialog.Builder(this.activity)
            .setTitle("Algo deu errado :(")
            .setMessage("Deseja tentar novamente?")
            .setPositiveButton("Sim") { _, _ ->
                onTryAgainObservable.onNext(movieId)
            }.setNegativeButton("Não") { _, _->
                exitProcess(0)
            }.create().show()
    }
}
package com.example.mpvmovies.data.cache

import com.example.mpvmovies.data.cache.model.MovieCM
import com.pacoworks.rxpaper2.RxPaperBook
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class MovieCDS @Inject constructor(private val rxPaperBook: RxPaperBook){
    companion object {
        const val MOVIE_LIST_KEY = "listMovies"
        const val FAVORITE_MOVIE_ID_LIST_KEY = "favoriteMovies"
    }
    //Mudar nome da cache e tirar o identifier
    fun getCachedData(): Single<List<MovieCM>> {
        return rxPaperBook.read(MOVIE_LIST_KEY)
    }

    fun setCachedData(movieList: List<MovieCM>): Completable {
        return rxPaperBook.write(MOVIE_LIST_KEY, movieList)
    }

    fun getFavoriteMovieList(): Single<MutableList<Int>> = rxPaperBook.read<MutableList<Int>>(FAVORITE_MOVIE_ID_LIST_KEY).onErrorReturn { mutableListOf() }

    fun favoriteMovie(idList: MutableList<Int>): Completable =  rxPaperBook.write(FAVORITE_MOVIE_ID_LIST_KEY, idList)
}
package com.example.mpvmovies.presentation.scene.movie.list

import io.reactivex.Observable

interface MovieListUI {
    fun showMovies(movie: List<MovieVM>)
    fun showProgress()
    fun hideProgress()
    fun showError()

    val onTryAgainObservable: Observable<Unit>
    val onMovieSelectedObservable: Observable<MovieVM>
    val onFavoriteMovieObservable: Observable<MovieVM>
}
package com.example.mpvmovies.data.mapper

import com.example.mpvmovies.data.cache.model.MovieCM
import com.example.mpvmovies.data.remote.model.MovieRM

fun List<MovieCM>.toMovieRM() = map{ it.toRM() }
fun MovieCM.toRM() = MovieRM(id, title, posterUrl, releaseDate)
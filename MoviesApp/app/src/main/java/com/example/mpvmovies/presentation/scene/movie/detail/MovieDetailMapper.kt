package com.example.mpvmovies.presentation.scene.movie.detail

import com.example.domain.model.MovieDetail

fun MovieDetail.toVM(): MovieDetailVM = MovieDetailVM(backPosterUrl, overview, voteAverage, genres, id, title, isFavorite, posterUrl, releaseDate)
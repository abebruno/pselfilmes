package com.example.mpvmovies.data.remote.model

import com.google.gson.annotations.SerializedName

data class MovieDetailRM(
    @SerializedName("backdrop_url")
    val backPosterUrl: String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("vote_average")
    val voteAverage: Double,

    val genres: List<String>?,
    val id: Int,
    val title: String,

    @SerializedName("poster_url")
    val posterUrl: String,

    @SerializedName("release_date")
    val releaseDate: String
)
package com.example.mpvmovies.data.mapper

import com.example.domain.model.Movie
import com.example.domain.model.MovieDetail
import com.example.mpvmovies.data.cache.model.MovieCM
import com.example.mpvmovies.data.cache.model.MovieDetailCM

fun List<MovieCM>.toMovieDM(favoriteIds: List<Int>) = map{ it.toDM(favoriteIds.contains(it.id)) }
fun MovieDetailCM.toDM(isFavorite: Boolean) = MovieDetail(backPosterUrl, overview, voteAverage, genres, id, title, isFavorite, posterUrl, releaseDate)
fun MovieCM.toDM(isFavorite: Boolean) = Movie(id, title, isFavorite, posterUrl, releaseDate)

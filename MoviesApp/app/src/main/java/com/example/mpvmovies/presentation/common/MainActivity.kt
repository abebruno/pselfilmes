package com.example.mpvmovies.presentation.common

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.mpvmovies.R
import com.example.mpvmovies.presentation.common.navigation.MoviesFlowFragment
import com.example.mpvmovies.presentation.scene.fake.FakeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        private const val movieFlowTAG: String = "1"
        private const val fakeFlowTAG: String = "2"
    }

    //No Lazy, é um ternário! Se achou a tag: retorna-a, cc: cria uma nova instancia
    private val moviesListFlowFragment: Fragment by lazy {
        supportFragmentManager.findFragmentByTag(movieFlowTAG) as? MoviesFlowFragment ?: MoviesFlowFragment()
    }
    private val fakeFragment: Fragment by lazy {
        supportFragmentManager.findFragmentByTag(fakeFlowTAG) as? FakeFragment ?: FakeFragment()
    }

    private var activeFragmentTag: String? = movieFlowTAG

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Transações da navBar
        setupNavActions()
    }

    override fun onBackPressed() {
        val currentFragmentMoviesFlow = (supportFragmentManager.findFragmentByTag(activeFragmentTag) as? BackButtonListener)
        currentFragmentMoviesFlow?.let {currentFragmentMoviesFlow ->
            if (!currentFragmentMoviesFlow.onBackPressed())
                finish()
        }
    }

    //Navegação na bottomNavigation, retorna true se o item foi selecionado, false cc
    private fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_home -> {
                supportFragmentManager.beginTransaction().hide(fakeFragment).show(moviesListFlowFragment).commit()
                activeFragmentTag = moviesListFlowFragment.tag
                true
            }
            R.id.navigation_dashboard -> {
                supportFragmentManager.beginTransaction().hide(moviesListFlowFragment).show(fakeFragment).commit()
                activeFragmentTag = fakeFragment.tag
                true
            }
            else -> {
                false
            }
        }
    }

    private fun setupNavActions(){
        supportFragmentManager.beginTransaction()
            .add(R.id.flContainerForFragment, fakeFragment, fakeFlowTAG)
            .add(R.id.flContainerForFragment, moviesListFlowFragment, movieFlowTAG)
            .hide(fakeFragment).commit()

        //Cliques na bottomNavigation
        bottomNavigationView.setOnNavigationItemSelectedListener { onNavigationItemSelected(it) }
    }

}

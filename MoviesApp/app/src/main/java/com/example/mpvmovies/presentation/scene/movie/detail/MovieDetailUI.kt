package com.example.mpvmovies.presentation.scene.movie.detail

import io.reactivex.Observable

interface MovieDetailUI{
    fun showDataInScreen(details: MovieDetailVM)
    fun hideProgress()
    fun showProgress()
    fun showError()

    val onTryAgainObservable: Observable<Int>
}
package com.example.mpvmovies.presentation.common.navigation


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.mpvmovies.R
import com.example.mpvmovies.presentation.common.BackButtonListener
import com.example.mpvmovies.presentation.common.MoviesApplication
import com.example.mpvmovies.presentation.common.NavigationKeys
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MoviesFlowFragment : Fragment(), BackButtonListener {
    @Inject
    lateinit var navigator: Navigator

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var router: Router

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_movies_flow, container, false)

        val component =
            DaggerMoviesFlowComponent
                .builder()
                .applicationComponent((activity?.application as? MoviesApplication)?.applicationComponent)
                .moviesFlowModule((activity)?.let { fragmentActivity ->  MoviesFlowModule(fragmentActivity , childFragmentManager)})
                .build()
        component.inject(this)

        if(savedInstanceState == null){
            this.router.replaceScreen(NavigationKeys.MoviesList())
        }

        return rootView
    }

    override fun onBackPressed(): Boolean {
        return if(isAdded) {
            val childFragment = childFragmentManager.findFragmentById(R.id.containerBaseMovieList)
            childFragment != null && childFragment is BackButtonListener && childFragment.onBackPressed()
        } else false
    }

    //onResume, onPause e onDestroy do cicerone
    override fun onPause() {
        super.onPause()
        this.navigatorHolder.removeNavigator()
    }

    override fun onResume() {
        super.onResume()
        this.navigatorHolder.setNavigator(this.navigator)
    }
}

package com.example.mpvmovies.data.mapper

import com.example.mpvmovies.data.cache.model.MovieCM
import com.example.mpvmovies.data.cache.model.MovieDetailCM
import com.example.mpvmovies.data.remote.model.MovieDetailRM
import com.example.mpvmovies.data.remote.model.MovieRM

fun List<MovieRM>.toMovieCM() = map{ it.toCM() }
fun MovieDetailRM.toCM() = MovieDetailCM(backPosterUrl, overview, voteAverage, genres, id, title, posterUrl, releaseDate)
fun MovieRM.toCM() = MovieCM(id, title, posterUrl, releaseDate)

package com.example.mpvmovies.presentation.common

import android.app.Application
import com.example.mpvmovies.presentation.common.di.ApplicationComponent
import com.example.mpvmovies.presentation.common.di.ApplicationModule
import com.example.mpvmovies.presentation.common.di.DaggerApplicationComponent

class MoviesApplication: Application() {
    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    /*
   companion object {
       lateinit var applicationComponent: ApplicationComponent
   }

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule())
            .build()
    }*/

}
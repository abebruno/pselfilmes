package com.example.mpvmovies.presentation.common.di

import javax.inject.Scope

@Scope
annotation class AppScope

@Scope
annotation class ActivityScope
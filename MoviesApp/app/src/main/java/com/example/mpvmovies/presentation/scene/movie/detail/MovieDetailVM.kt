package com.example.mpvmovies.presentation.scene.movie.detail

import java.io.Serializable

data class MovieDetailVM(
    val backPosterUrl: String,
    val overview: String,
    val voteAverage: Double,
    val genres: List<String>?,
    val id: Int,
    val title: String,
    val isFavorite: Boolean,
    val posterUrl: String,
    val releaseDate: String
): Serializable
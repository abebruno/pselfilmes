package com.example.mpvmovies.presentation.common

import android.os.Bundle
import com.evernote.android.state.Bundler
import com.example.mpvmovies.presentation.common.livedata.StateEventLiveData
import java.io.Serializable

//State custom para usar a biblioteca do evernote
class BundlerLiveData<T : Serializable> : Bundler<StateEventLiveData<T>> {
    override fun put(key: String, value: StateEventLiveData<T>, bundle: Bundle) {
        value.value.let { bundle.putSerializable(key, it) }
    }

    override fun get(key: String, bundle: Bundle): StateEventLiveData<T> {
        val liveData = StateEventLiveData<T>()

        val value = bundle.getSerializable(key)
        value?.let { liveData.value = it as? StateEvent<T> }

        return liveData
    }
}
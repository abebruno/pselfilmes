package com.example.mpvmovies.data.cache.model

import com.google.gson.annotations.SerializedName

data class MovieDetailCM(
    @SerializedName("backdrop_url")
    val backPosterUrl: String,

    @SerializedName("overview")
    val overview: String,

    @SerializedName("vote_average")
    val voteAverage: Double,

    val genres: List<String>?,
    val id: Int,
    val title: String,
    val posterUrl: String,
    val releaseDate: String
)